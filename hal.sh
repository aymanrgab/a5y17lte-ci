#!/bin/bash
source a7y17lte.env
cd $ANDROID_ROOT
set -xe
apt update
apt-get install -y \
git gnupg flex ccache bison gperf build-essential \
zip bzr curl libc6-dev x11proto-core-dev \
libgl1-mesa-dev g++-multilib tofrodos \
python3-markdown libxml2-utils xsltproc schedtool \
liblz4-tool bc lzop imagemagick libncurses5 rsync \
python-is-python3 cpio wget
wget 'https://storage.googleapis.com/git-repo-downloads/repo' -P /bin/
chmod +x /bin/repo
git config --global user.name "aymanrgab"
git config --global user.email "aymanrar2c@gmail.com"
echo "updated"
repo init -u https://github.com/mer-hybris/android.git -b hybris-17.1 --depth=1
echo "repo init"
git clone https://github.com/Exynos7880-Linux/local_manifests.git .repo/local_manifests -b hybris-17.1
rm .repo/local_manifests/a5y17lte.xml
repo sync --fetch-submodules
echo "done"
./hybris-patches/apply-patches.sh --mb
export TEMPORARY_DISABLE_PATH_RESTRICTIONS=true
export USE_CCACHE=1
export ANDROID_MAJOR_VERSION=o
source build/envsetup.sh 2>&1
breakfast $DEVICE

echo "clean .repo folder"
#rm -rf $ANDROID_ROOT/.repo

make -j$(nproc --all) hybris-hal hybris-boot droidmedia libbiometry_fp_api fake_crypt
hybris/mw/sailfish-fpd-community/rpm/copy-hal.sh
exit
