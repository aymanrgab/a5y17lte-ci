#!/bin/sh
source a7y17lte.env
sudo zypper in -y kmod 
sudo zypper -n in lvm2

echo "Creating loop devices..."
sudo mknod /dev/loop0 -m0660 b 7 0
sudo mknod /dev/loop1 -m0660 b 7 1
sudo mknod /dev/loop2 -m0660 b 7 2
echo $?
ls -lh /dev/loop*

echo "Building Sailfish $RELEASE"
cd ${ANDROID_ROOT}

rpm/dhd/helpers/build_packages.sh -d
rpm/dhd/helpers/build_packages.sh -c
rpm/dhd/helpers/build_packages.sh -m
rpm/dhd/helpers/build_packages.sh -g
rpm/dhd/helpers/build_packages.sh -v
rpm/dhd/helpers/build_packages.sh -i
rpm/dhd/helpers/build_packages.sh --build=hybris/mw/sailfish-fpd-community --spec=rpm/droid-biometry-fp.spec --do-not-install
rpm/dhd/helpers/build_packages.sh --build=hybris/mw/sailfish-fpd-community --spec=rpm/droid-fake-crypt.spec --do-not-install
rpm/dhd/helpers/build_packages.sh --build=hybris/mw/sailfish-fpd-community
zip -r sfe-a7y17lte.zip "${ANDROID_ROOT?}/droid-local-repo/${DEVICE?}"

sudo zypper in -y kmod 
#sudo mic create fs --arch=$PORT_ARCH \
#--tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,EXTRA_NAME:$EXTRA_NAME \
#--record-pkgs=name,url \
#--outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
#--pack-to=sfe-$DEVICE-$RELEASE$EXTRA_NAME.tar.bz2 \
#Jolla-@RELEASE@-$DEVICE-@ARCH@.ks
